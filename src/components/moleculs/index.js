import Header from './header';
import BottomNavigator from './BottomNavigator';
import HomeProfile from './HomeProfile';
import DoctorCategory from './DoctorCategory';
import RatedDoctor from './RatedDoctor';
import NewsItem from './NewsItem';
import ListDoctor from './ListDoctor';
import ListHospitals from './ListHospitals';
import ChatItem from './ChatItem';
import InputChat from './InputChat';

export {
  Header,
  BottomNavigator,
  HomeProfile,
  DoctorCategory,
  RatedDoctor,
  NewsItem,
  ListDoctor,
  ListHospitals,
  ChatItem,
  InputChat,
};
