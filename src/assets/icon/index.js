import IconBackDark from './arrow.svg';
import IconAddfoto from './btn_add_photo.svg';
import IconRemovefoto from './btn_remove_photo.svg';
import IconDoctor from './face_unlock_nonactive.svg';
import IconDoctorActive from './face_unlock_active.svg';
import IconHospitals from './hospitals_nonactive.svg';
import IconHospitalsActive from './hospitals_active.svg';
import IconMsg from './msg_nonactive.svg';
import IconMsgActive from './msg_active.svg';
import IconStar from './star.svg';
import IconButtonRight from './button_right.svg';
import IconBackLight from './back_light.svg';
export {
  IconBackDark,
  IconAddfoto,
  IconRemovefoto,
  IconDoctor,
  IconDoctorActive,
  IconHospitals,
  IconHospitalsActive,
  IconMsg,
  IconMsgActive,
  IconStar,
  IconButtonRight,
  IconBackLight,
};
