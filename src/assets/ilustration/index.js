import ILLogo from './logo.svg';
import ILGetstarted from './get-started.png';
import ILNullfoto from './null_foto.png';
import ILCatUmum from './icon_dokter_umum.svg';
import ILCatSikiater from './icon_dokter_sikiater.svg';
import ILCatAnak from './icon_dokter_anak.svg';
import ILCatObat from './icon_dokter_obat.svg';
import ILHospitals from './bg_hospitals.png';

export {
  ILLogo,
  ILGetstarted,
  ILNullfoto,
  ILCatUmum,
  ILCatSikiater,
  ILCatAnak,
  ILCatObat,
  ILHospitals,
};
