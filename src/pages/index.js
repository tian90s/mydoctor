import Splash from './Splash';
import GetStarted from './GetStarted';
import Register from './Register';
import Login from './Login';
import UploadFoto from './UploadFoto';
import Doctor from './Doctor';
import Messages from './Messages';
import Hostpitals from './Hospitals';
import ChooseDoctor from './ChooseDoctor';
import Chatting from './Chatting';

export {
  Splash,
  GetStarted,
  Register,
  Login,
  UploadFoto,
  Doctor,
  Messages,
  Hostpitals,
  ChooseDoctor,
  Chatting,
};
