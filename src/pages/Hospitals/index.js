import React from 'react';
import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import {
  DummyHospitals1,
  DummyHospitals2,
  DummyHospitals3,
  ILHospitals,
} from '../../assets';
import {ListHospitals} from '../../components';
import {colors, fonts} from '../../utils';

export default function Hostpitals() {
  return (
    <View style={styles.page}>
      <ImageBackground source={ILHospitals} style={styles.background}>
        <Text style={styles.title}>Nearby Hospitals</Text>
        <Text style={styles.desc}>3 tersedia</Text>
      </ImageBackground>
      <View style={styles.content}>
        <ListHospitals
          type="Rumah Sakit"
          name="Citra Bunga Medika"
          address="Jln. Surya Sejahtera 20"
          pic={DummyHospitals1}
        />
        <ListHospitals
          type="Rumah Sakit Anak"
          name="Happy Family & Kids"
          address="Jln. Surya Sejahtera 20"
          pic={DummyHospitals2}
        />
        <ListHospitals
          type="Rumah Sakit Jiwa"
          name="Tingkatan Paling Atas"
          address="Jln. Surya Sejahtera 20"
          pic={DummyHospitals3}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  background: {
    height: 240,
    paddingTop: 30,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.white,
    textAlign: 'center',
  },
  desc: {
    fontSize: 14,
    fontFamily: fonts.primary[300],
    color: colors.white,
    marginTop: 6,
    textAlign: 'center',
  },
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    flex: 1,
    borderRadius: 20,
    marginTop: -30,
    paddingTop: 16,
  },
});
