import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {Header, Button, Link, Gap} from '../../components';
import {ILNullfoto, IconAddfoto} from '../../assets';
import {colors, fonts} from '../../utils';

export default function UploadFoto({navigation}) {
  return (
    <View style={styles.page}>
      <Header onPress={() => navigation.goBack()} title="Upload Foto" />
      <View style={styles.content}>
        <View style={styles.profile}>
          <View style={styles.avatarWrapper}>
            <Image source={ILNullfoto} style={styles.avatar} />
            <IconAddfoto style={styles.addFoto} />
          </View>
          <Text style={styles.text}>Shayna Melinda</Text>
          <Text style={styles.profesion}>Product Designer</Text>
        </View>
        <View>
          <Button title="Upload And Continue" />
          <Gap height={30} />
          <Link title="Skip For This" align="center" size={16} />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  profile: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  avatar: {
    width: 110,
    height: 110,
  },
  avatarWrapper: {
    width: 130,
    height: 130,
    borderWidth: 1,
    borderColor: colors.border,
    borderRadius: 130 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addFoto: {
    position: 'absolute',
    bottom: 8,
    right: 6,
  },
  text: {
    fontSize: 24,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    textAlign: 'center',
  },
  profesion: {
    fontSize: 18,
    fontFamily: fonts.primary.normal,
    textAlign: 'center',
    color: colors.text.secondary,
    marginTop: 4,
  },
  content: {
    paddingHorizontal: 40,
    flex: 1,
    justifyContent: 'space-between',
    paddingBottom: 64,
  },
});
