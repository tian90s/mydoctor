import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ChatItem, InputChat, Header} from '../../components';
export default function Chatting({type}) {
  return (
    <View>
      <Header title="Nairobi Putri Hayza" type="dark-profile" />
      <Text>Senin, 21 Maret, 2020</Text>
      <ChatItem />
      <ChatItem />
      <ChatItem />
      <InputChat />
    </View>
  );
}

const styles = StyleSheet.create({});
